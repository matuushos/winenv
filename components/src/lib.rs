use egui::{Grid, Image};
use rand::Rng;
pub struct App<'a> {
    name: String,
    icon: egui::Image<'a> // Assuming you have loaded the icon image as a texture
}

impl App {
    pub fn new() -> Self {
        Self {
            name: "".to_string(),
            icon: Image::new("")
        }
    }
    pub fn spawn(self) {
        let mut grid = Grid::new(Some(false))
            .cols(5) // Adjust cols for desired number of apps per row
            .spacing(10.0);

        for app in &apps {
            let 
            grid.add(|| {
                // Add your app UI here (icon, name, etc.)
                egui::Image::new(&app.icon, egui::Vec2::new(50.0, 50.0));
                ui.label(&app.name);
            });
        }

        ui.add(&grid);
    }
}